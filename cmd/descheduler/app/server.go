/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package app implements a Server object for running the descheduler.
package app

import (
	"context"
	"io"
	"os/signal"
	"syscall"

	"k8s.io/apiserver/pkg/server/healthz"

	"sigs.k8s.io/descheduler/cmd/descheduler/app/options"
	"sigs.k8s.io/descheduler/pkg/descheduler"

	"github.com/spf13/cobra"

	apiserver "k8s.io/apiserver/pkg/server"
	"k8s.io/apiserver/pkg/server/mux"
	restclient "k8s.io/client-go/rest"
	"k8s.io/component-base/config"
	_ "k8s.io/component-base/logs/json/register"
	"k8s.io/component-base/logs/registry"
	"k8s.io/component-base/metrics/legacyregistry"
	"k8s.io/klog/v2"
)

// NewDeschedulerCommand creates a *cobra.Command object with default parameters
func NewDeschedulerCommand(out io.Writer) *cobra.Command {
	// 新建一个重调度服务
	s, err := options.NewDeschedulerServer()

	if err != nil {
		klog.ErrorS(err, "unable to initialize server")
	}

	cmd := &cobra.Command{
		Use:   "descheduler",
		Short: "descheduler",
		Long:  `The descheduler evicts pods which may be bound to less desired nodes`,
		Run: func(cmd *cobra.Command, args []string) {
			// s.Logs.Config.Format = s.Logging.Format

			// LoopbackClientConfig is a config for a privileged loopback connection
			// 创建一个回环连接，通过client_go创建一个客户端，通过apiserver创建服务端
			var LoopbackClientConfig *restclient.Config
			var SecureServing *apiserver.SecureServingInfo
			// 对重调度服务s设置客户端和服务端
			if err := s.SecureServing.ApplyTo(&SecureServing, &LoopbackClientConfig); err != nil {
				klog.ErrorS(err, "failed to apply secure server configuration")
				return
			}

			factory, _ := registry.LogRegistry.Get(s.Logging.Format)
			if factory == nil {
				klog.ClearLogger()
			} else {
				log, logrFlush := factory.Create(config.LoggingConfiguration{})
				defer logrFlush()
				klog.SetLogger(log)
			}
			// 建立一个方便优雅重启服务的ctx。syscall是系统或者父进程传输给我们的信号，比如执行了ctrl+C，或者父进程被优雅杀死，那就要把
			// 这个信号传输下去。执行done就是不再捕获注册的信号，算是释放资源
			ctx, done := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)


			// 跟HTTP请求相关。HTTP抽象复用。
			pathRecorderMux := mux.NewPathRecorderMux("descheduler")
			if !s.DisableMetrics {
				pathRecorderMux.Handle("/metrics", legacyregistry.HandlerWithReset())
			}


			// healthz包用于实现 liveness 、readiness 探针。
			healthz.InstallHandler(pathRecorderMux, healthz.NamedCheck("Descheduler", healthz.PingHealthz.Check))

			// 让服务端来开始服务这个客户端请求。其中 pathRecorderMux 是连接主体。0 是个参数，或许是超时时间。ctx用来将入子go runline
			stoppedCh, _, err := SecureServing.Serve(pathRecorderMux, 0, ctx.Done())
			if err != nil {
				klog.Fatalf("failed to start secure server: %v", err)
				return
			}

			// 对s真正进行服务
			err = Run(ctx, s)
			if err != nil {
				klog.ErrorS(err, "descheduler server")
			}

			// 比如在应用逻辑执行完了之后就会执行done()。不再捕获注册的信号，释放资源。
			done()

			// 等待服务结束
			// wait for metrics server to close
			<-stoppedCh
		},
	}
	cmd.SetOut(out)
	flags := cmd.Flags()
	s.AddFlags(flags)
	return cmd
}

func Run(ctx context.Context, rs *options.DeschedulerServer) error {
	return descheduler.Run(ctx, rs)
}
